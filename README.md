# Scraper for rejestradwokatow.pl

How to use

1. Get pages with table (pagination with 272 pages, 100 items per page)

This command will download all pages in table to computer

```
mkdir -p raw && for i in {1..272}; do wget "https://rejestradwokatow.pl/adwokat/wyszukaj/strona/$i" -O raw/$i.html; done
```

2. Process table data

This will gets all files with tables in `raw` dir and generate `out/basic_data.json`

```
ts-node entry.ts
```

basic data has structure:

```
Output {
    surname: string
    name: string
    second_name: string
    city: string
    office: string
    status: string
    link: string
}
```

3. Download all links to computer

```
ts-node scraper.ts
```

Generate files like `raw/konrady-anna-81779.html` In this case over 27k files in about 1hour

4. Process all files

```
time ts-node parser.ts
```

It will extend data by

```
id - licence number of lawyer like KRA/Adw/3642
```

If status is "Niewykonujący zawodu" then there will be added

```
data - Data wpisu w aktualnej izbie na listę adwokatów
```

If status is

```
ExtraOutput {
    date: string
    address: string
    phone: string
    email: string
    workplace: string
    speciality: string[]
}
```